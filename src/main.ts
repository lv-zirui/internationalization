import i18n from '@/locale'
import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App)

app.use(i18n).mount('#app')
