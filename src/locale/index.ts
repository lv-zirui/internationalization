import { createI18n } from 'vue-i18n'
import zhCN from './lang/zh-CN' // 中文
import enUS from './lang/en-US' // 英文
import ptBR from './lang/pt-BR' // 西班牙语
import ja from './lang/ja' // 日语

export const LOCALE_OPTIONS = [
  { label: '中文', value: 'zh-CN' },
  { label: '英文', value: 'en-US' },
  { label: '西班牙语', value: 'pt-BR' },
  { label: '日语', value: 'ja' }
];
const defaultLocale = localStorage.getItem('locale') || 'en-US';
const i18n = createI18n({
  locale: defaultLocale,
  legacy: false, // 解决控制台报错 Uncaught SyntaxError: Not available in legacy mode
  messages: {
    'zh-CN': zhCN,
    'en-US': enUS,
    'pt-BR': ptBR,
    'ja': ja
  }
})

export default i18n