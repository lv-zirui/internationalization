export default {
  "menu": {
    "btn": "Botones",
    "upload": "Cargar",
    "index": "Número de serie",
    "activityName": "Nombre del evento",
    "activityCountdown": "Cuenta atrás para actividades",
    "days": 'Días',
    "hours": 'Horas',
    "minutes": 'Min',
    "seconds": 'Segundos',
    "postCancellation": "Cancelación posterior",
    "status": "El evento ha terminado",
    "activity": "Actividades"
  }
}