export default {
  "menu": {
    "btn": "ボタンを使用します。",
    "upload": "アップロード",
    "index": "シーケンス番号",
    "activityName": "アクティビティ名",
    "activityCountdown": "アクティビティのカウントダウン",
    "days": '日',
    "hours": '時間',
    "minutes": '分',
    "seconds": '秒',
    "postCancellation": "ポストキャンセル",
    "status": "アクティビティが終了しました",
    "activity": "アクティビティ"
  }
}