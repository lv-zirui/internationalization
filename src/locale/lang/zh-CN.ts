export default {
  "menu": {
    "btn": "按钮",
    "upload": "上传",
    "index": "序号",
    "activityName": "活动名称",
    "activityCountdown": "活动倒计时",
    "days": '天',
    "hours": '小时',
    "minutes": '分钟',
    "seconds": '秒',
    "postCancellation": "后自动取消",
    "activity": "活动",
    "status": "活动已结束"
  }
}