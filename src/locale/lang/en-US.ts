export default {
  "menu": {
    "btn": "button",
    "upload": "upload",
    "index": "Number",
    "activityName": "Activity Name",
    "activityCountdown": "Activity Countdown",
    "days": 'day',
    "hours": 'hour',
    "minutes": 'minute',
    "seconds": 'second',
    "postCancellation": "Post cancellation",
    "status": "The activity has ended",
    "activity": "activity"
  }
}